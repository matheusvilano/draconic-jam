// This work by Matheus Vilano, Rohit Mukherjee, et al.is licensed under Attribution - NonCommercial - ShareAlike 4.0 International.To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-sa/4.0/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DraconicJamGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DRACONICJAM_API ADraconicJamGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
