// This work by Matheus Vilano, Rohit Mukherjee, et al.is licensed under Attribution - NonCommercial - ShareAlike 4.0 International.To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-sa/4.0/

using UnrealBuildTool;

public class DraconicJam : ModuleRules
{
	public DraconicJam(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] { "FMODStudio" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
