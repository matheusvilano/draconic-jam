// This work by Matheus Vilano, Rohit Mukherjee, et al.is licensed under Attribution - NonCommercial - ShareAlike 4.0 International.To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-sa/4.0/

using UnrealBuildTool;
using System.Collections.Generic;

public class DraconicJamEditorTarget : TargetRules
{
	public DraconicJamEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "DraconicJam" } );
	}
}
